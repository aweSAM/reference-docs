# reference-docs

* https://www.twblogs.net/a/5b8dfb6e2b717718834206ed (in Traditional Chinese)
* https://rocmdocs.amd.com/en/latest/Installation_Guide/Installation-Guide.html#id7
* https://www.preining.info/blog/2020/05/switching-from-nvidia-to-amd-including-tensorflow/

Terminations:
* ROC —— Radeon Open Computing
* ROCm——ROC platforM
* HSA——Heterogeous system architecturentime
* GCN——Graphics Core Next

| CUDA | ROCm | comment |
| ---      |  ------  |---------:|
| CUDA API   | HIP   | C++ extension   |
| NVCC | HCC / hip-clang | compiler |
| cuda libs   |    roc,hc libs      |  |
| Thrust	| Parallel STL	| HCC native support |
| Profiler	| ROCm Profiler | |	 
| CUDA-GDB	| ROCm-GDB| |	 
| nvidia-smi	| rocm-smi	| | 
| DirectGPU RDMA	| ROCm RDMA	| peer2peer |
| TensorRT	| Tensile	| tensor computation lib|
| CUDA-Docker	| ROCm-Docker| |
| cuDNN | miopen | deep neural network |
| nccl | rccl | distributed |
| ? | amdkfd | prerequisite kernel module |
| ? | libhsakmt | roct-thunk-interface |
| ? | ? | ROCr runtime hsa-rocr-dev |
| ? | rocm-cmake | dev |
| ? | rocminfo | |
| ? | rocm-device-libs | ROCm device libraries |
| nvidia-opencl-icd |  ROCm OpenCL: rocm-opencl, rocm-opencl-devel (on RHEL/CentOS), rocm-opencl-dev (on Ubuntu) | |
| ? |  ROCM Clang-OCL Kernel Compiler: rocm-clang-ocl | |



        Asynchronous Task and Memory Interface (ATMI): atmi

        ROCm Debug Agent: rocm_debug_agent

        ROCm Code Object Manager: comgr

        ROC Profiler: rocprofiler-dev

        ROC Tracer: roctracer-dev

        Radeon Compute Profiler: rocm-profiler

        rocALUTION: rocalution

        rocBLAS: rocblas

        hipBLAS: hipblas

        hipCUB: hipCUB

        rocFFT: rocfft

        rocRAND: rocrand

        rocSPARSE: rocsparse

        hipSPARSE: hipsparse

        ROCm SMI Lib: rocm-smi-lib64

        rocThrust: rocThrust

        MIOpen: MIOpen-HIP (for the HIP version), MIOpen-OpenCL (for the OpenCL version)

        MIOpenGEMM: miopengemm

        MIVisionX: mivisionx

        RCCL: rccl

